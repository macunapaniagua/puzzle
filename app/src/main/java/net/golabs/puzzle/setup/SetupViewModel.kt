package net.golabs.puzzle.setup

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import net.golabs.puzzle.R
import net.golabs.puzzle.models.PuzzlePreview

class SetupViewModel : ViewModel() {

    private val viewModel = MutableLiveData<List<PuzzlePreview>>()
    fun getViewModel(): LiveData<List<PuzzlePreview>> = viewModel

    init {
        viewModel.value = getAvailableBackgrounds()
    }

    private fun getAvailableBackgrounds(): List<PuzzlePreview> {
        return listOf(
            PuzzlePreview(true, R.drawable.bg_canyon_1),
            PuzzlePreview(false, R.drawable.bg_canyon_2),
            PuzzlePreview(false, R.drawable.bg_clouds),
            PuzzlePreview(false, R.drawable.bg_clouds_mage),
            PuzzlePreview(false, R.drawable.bg_mountain),
            PuzzlePreview(false, R.drawable.bg_mario_kart)
        )
    }

}
