package net.golabs.puzzle.setup.adapters

import android.view.View
import android.view.ViewGroup
import androidx.annotation.DrawableRes
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.puzzle_preview_view.view.*
import net.golabs.puzzle.R
import net.golabs.puzzle.extensions.inflate
import net.golabs.puzzle.models.PuzzlePreview

class PuzzlePreviewAdapter(private var items: List<PuzzlePreview>): RecyclerView.Adapter<PuzzlePreviewViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): PuzzlePreviewViewHolder {
        return PuzzlePreviewViewHolder(parent.inflate(R.layout.puzzle_preview_view, false))
    }

    override fun getItemCount(): Int = items.size

    override fun onBindViewHolder(holder: PuzzlePreviewViewHolder, position: Int) {
        with (items[position]) {
            holder.bind(this)
            holder.itemView.setOnClickListener { refreshItems(position) }
        }
    }

    private fun refreshItems(selectedPosition: Int) {
        // Uncheck any selected preview
        items.withIndex().filter {
            it.value.isSelected
        }.map {
            items[it.index].isSelected = false
            notifyItemChanged(it.index)
        }
        // Check the new selected Preview
        items[selectedPosition].isSelected = true
        notifyItemChanged(selectedPosition)
    }

    fun setItems(items: List<PuzzlePreview>) {
        this.items = items
        notifyDataSetChanged()
    }

    @DrawableRes
    fun getSelectedImage(): Int {
        return items.first { it.isSelected }.imageResId
    }

}

class PuzzlePreviewViewHolder(view: View): RecyclerView.ViewHolder(view) {

    fun bind(puzzlePreview: PuzzlePreview) {
        itemView.preview_image.setImageResource(puzzlePreview.imageResId)
        val border = if (puzzlePreview.isSelected) R.drawable.bg_selected_preview else 0
        itemView.preview_image.setBackgroundResource(border)
    }

}


