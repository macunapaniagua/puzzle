package net.golabs.puzzle.setup

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.SimpleItemAnimator
import kotlinx.android.synthetic.main.setup_fragment.*
import net.golabs.puzzle.R
import net.golabs.puzzle.models.PuzzlePreview
import net.golabs.puzzle.setup.adapters.PuzzlePreviewAdapter
import net.golabs.puzzle.utils.ItemOffsetDecoration

class SetupFragment : Fragment() {

    private lateinit var viewModel: SetupViewModel
    private var previewsAdapter = PuzzlePreviewAdapter(emptyList())

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.setup_fragment, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        initViews()
        viewModel = ViewModelProviders.of(this).get(SetupViewModel::class.java)
        viewModel.getViewModel().observe(this, Observer(::refreshPuzzlePreviews))
        setListeners()
    }

    private fun initViews() {
        with (previews) {
            adapter = previewsAdapter
            addItemDecoration(ItemOffsetDecoration(resources.getDimensionPixelSize(R.dimen.setup_preview_offset)))
            (itemAnimator as SimpleItemAnimator).supportsChangeAnimations = false
        }
    }

    private fun setListeners() {
        startGame.setOnClickListener {
            val gridDimension = getSelectedDimension()
            val pictureResId = previewsAdapter.getSelectedImage()
            val action = SetupFragmentDirections.actionSetupFragmentToPuzzleFragment(pictureResId, gridDimension)
            findNavController().navigate(action)
        }
    }

    private fun refreshPuzzlePreviews(puzzlePreviews: List<PuzzlePreview>) {
        previewsAdapter.setItems(puzzlePreviews)

    }

    private fun getSelectedDimension(): Int {
        return when (dimensions.checkedRadioButtonId) {
            dimension_2_x_2.id -> 2
            dimension_3_x_3.id -> 3
            dimension_4_x_4.id -> 4
            dimension_5_x_5.id -> 5
            dimension_6_x_6.id -> 6
            else -> 2
        }
    }

}
