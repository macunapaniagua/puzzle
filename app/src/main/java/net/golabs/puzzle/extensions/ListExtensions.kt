package net.golabs.puzzle.extensions

fun List<Int>.isSorted(): Boolean {
    return this.toIntArray().contentEquals(this.sorted().toIntArray())
}