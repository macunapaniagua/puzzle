package net.golabs.puzzle.extensions

fun Int.isOdd(): Boolean {
    return this % 2 == 1
}

fun Int.isEven(): Boolean {
    return this % 2 == 0
}