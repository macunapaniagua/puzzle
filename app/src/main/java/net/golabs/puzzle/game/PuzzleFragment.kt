package net.golabs.puzzle.game

import android.graphics.BitmapFactory
import androidx.lifecycle.ViewModelProviders
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.Observer
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import androidx.recyclerview.widget.GridLayoutManager
import kotlinx.android.synthetic.main.puzzle_fragment.*
import net.golabs.puzzle.R
import net.golabs.puzzle.game.adapters.PuzzleBoardAdapter
import net.golabs.puzzle.models.Puzzle
import net.golabs.puzzle.utils.ItemOffsetDecoration

class PuzzleFragment : Fragment() {

    private val args: PuzzleFragmentArgs by navArgs()
    private lateinit var viewModel: PuzzleViewModel
    private lateinit var boardAdapter: PuzzleBoardAdapter

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.puzzle_fragment, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        initViews()
        initViewModel()
        startPuzzle()
    }

    private fun initViews() {
        boardAdapter = PuzzleBoardAdapter(emptyList(), ::onPieceClicked)
        puzzleBoard.also {
            it.adapter = boardAdapter
            it.layoutManager = GridLayoutManager(context, args.gridDimension)
            val itemDecoration = ItemOffsetDecoration(resources.getDimensionPixelSize(R.dimen.puzzle_piece_offset))
            it.addItemDecoration(itemDecoration)
        }
        finishButton.setOnClickListener { findNavController().navigateUp() }
        startOverButton.setOnClickListener { startOver() }
    }

    private fun initViewModel() {
        viewModel = ViewModelProviders.of(this).get(PuzzleViewModel::class.java)
        viewModel.getPuzzleObservable().observe(this, Observer(::refreshBoard))
        viewModel.getTimerObservable().observe(this, Observer(::refreshTimer))
        viewModel.getPuzzleSolvedObservable().observe(this, Observer(::refreshPuzzleSolved))
    }

    private fun startPuzzle() {
        val bitmap = BitmapFactory.decodeResource(resources, args.pictureResId)
        viewModel.startGame(bitmap, args.gridDimension)
    }

    private fun startOver() {
        val bitmap = BitmapFactory.decodeResource(resources, args.pictureResId)
        viewModel.startOver(bitmap, args.gridDimension)
    }

    private fun refreshPuzzleSolved(isPuzzleSolved: Boolean) {
        buttonsGroup.visibility = if (isPuzzleSolved) View.VISIBLE else View.INVISIBLE
    }

    private fun refreshTimer(time: String) {
        puzzleTimer.text = time
    }

    private fun refreshBoard(puzzle: Puzzle) {
        boardAdapter.setItems(puzzle.pieces.flatten())
    }

    private fun onPieceClicked(position: Int) {
        viewModel.movePiece(position)
    }

}
