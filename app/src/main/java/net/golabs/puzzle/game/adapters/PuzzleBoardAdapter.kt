package net.golabs.puzzle.game.adapters

import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.puzzle_piece_view.view.*
import net.golabs.puzzle.R
import net.golabs.puzzle.extensions.inflate
import net.golabs.puzzle.models.Piece

class PuzzleBoardAdapter(private var items: List<Piece>,
                         private val clickListener: (Int) -> Unit): RecyclerView.Adapter<PuzzleBoardViewHolder>() {

    override fun getItemCount(): Int = items.size

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): PuzzleBoardViewHolder {
        return PuzzleBoardViewHolder(parent.inflate(R.layout.puzzle_piece_view, false))
    }

    override fun onBindViewHolder(holder: PuzzleBoardViewHolder, position: Int) {
        with (items[position]) {
            holder.bind(this)
            holder.itemView.setOnClickListener {
                clickListener.invoke(position)
            }
        }
    }

    fun setItems(items: List<Piece>) {
        this.items = items
        notifyDataSetChanged()
    }

}

class PuzzleBoardViewHolder(view: View): RecyclerView.ViewHolder(view) {

    fun bind(piece: Piece) {
        itemView.piece_image.setImageBitmap(piece.bitmap)
    }

}