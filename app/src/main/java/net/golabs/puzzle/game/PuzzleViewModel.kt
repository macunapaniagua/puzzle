package net.golabs.puzzle.game

import android.graphics.Bitmap
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import net.golabs.puzzle.models.Puzzle
import java.util.*
import java.util.concurrent.TimeUnit
import kotlin.concurrent.schedule

class PuzzleViewModel : ViewModel() {

    private val timerDelay: Long = 100L
    private lateinit var timer: Timer
    private val timerObservable = MutableLiveData<String>()
    private val puzzleObservable = MutableLiveData<Puzzle>()
    private val puzzleSolvedObservable = MutableLiveData<Boolean>()

    fun startGame(bitmap: Bitmap, gridDimension: Int) {
        if (puzzleObservable.value == null) {
            createGame(bitmap, gridDimension)
        }
    }

    fun startOver(bitmap: Bitmap, gridDimension: Int) {
        createGame(bitmap, gridDimension)
    }

    private fun createGame(bitmap: Bitmap, gridDimension: Int) {
        puzzleObservable.value = Puzzle(gridDimension, bitmap).apply { generateGame() }
        puzzleSolvedObservable.value = false
        startTimer()
    }

    private fun startTimer() {
        val startTime = System.currentTimeMillis()
        timer = Timer()
        timer.schedule(0, timerDelay) {
            timerObservable.postValue(getElapsedTime(startTime))
        }
    }

    private fun getElapsedTime(startTime: Long): String {
        val elapsedTime = System.currentTimeMillis() - startTime
        return String.format("%02d:%02d.%03d",
            TimeUnit.MILLISECONDS.toMinutes(elapsedTime) % 60,
            TimeUnit.MILLISECONDS.toSeconds(elapsedTime) % 60,
            TimeUnit.MILLISECONDS.toMillis(elapsedTime) % 1000)
    }

    private fun stopTimer() {
        timer.cancel()
    }

    fun movePiece(position: Int) {
        val puzzle = puzzleObservable.value ?: return
        if (puzzle.movePiece(position)) {
            if (puzzle.isSolved) {
                stopTimer()
                puzzleSolvedObservable.value = true
            }
            puzzleObservable.value = puzzle
        }
    }

    fun getTimerObservable(): LiveData<String> = timerObservable
    fun getPuzzleObservable(): LiveData<Puzzle> = puzzleObservable
    fun getPuzzleSolvedObservable(): LiveData<Boolean> = puzzleSolvedObservable

    override fun onCleared() {
        super.onCleared()
        stopTimer()
    }

}
