package net.golabs.puzzle.enums

enum class Position {
    LEFT, RIGHT, TOP, BOTTOM
}