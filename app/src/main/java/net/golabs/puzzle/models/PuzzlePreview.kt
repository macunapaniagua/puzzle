package net.golabs.puzzle.models

import androidx.annotation.DrawableRes

data class PuzzlePreview(var isSelected: Boolean, @DrawableRes val imageResId: Int)