package net.golabs.puzzle.models

import android.graphics.Bitmap
import net.golabs.puzzle.enums.Position
import net.golabs.puzzle.extensions.isEven
import net.golabs.puzzle.extensions.isOdd
import net.golabs.puzzle.extensions.isSorted

class Puzzle(private val dimension: Int,
             private val background: Bitmap) {

    lateinit var pieces: List<MutableList<Piece>>
    var isSolved: Boolean = false
        private set

    fun generateGame() {
        isSolved = false
        pieces = generatePieces()
            .shufflePieces()
            .chunked(dimension)
            .map { it.toMutableList() }
    }

    // region Create Pieces

    private fun generatePieces(): List<Piece> {
        val totalPieces = dimension.times(dimension).minus(1)
        return generateImages(background, dimension).mapIndexed { index, bitmapChunk ->
            val bitmap = if (index < totalPieces) bitmapChunk else null
            val positionIn2DArray = findPositionIn2DArray(index)
            Piece(bitmap, index.plus(1), positionIn2DArray.first, positionIn2DArray.second)
        }
    }

    private fun generateImages(bitmap: Bitmap, gridDimension: Int): List<Bitmap> {
        // Calculate size of the chunk
        val chunkHeight = bitmap.height / gridDimension
        val chunkWidth = bitmap.width / gridDimension
        // Split the original image into chunks
        val bitmapChunks = mutableListOf<Bitmap>()
        repeat(gridDimension) { row ->
            val startY = row * chunkHeight
            repeat(gridDimension) { col ->
                val startX = col * chunkWidth
                val chunk = Bitmap.createBitmap(bitmap, startX, startY, chunkWidth, chunkHeight)
                bitmapChunks.add(chunk)
            }
        }
        return bitmapChunks
    }

    // endregion

    // region Shuffle Pieces

    private fun List<Piece>.shufflePieces(): List<Piece> {
        var shuffledPieces: List<Piece>
        do {
            shuffledPieces = this.shuffled()
        } while (!isSolvable(shuffledPieces))
        return shuffledPieces
    }

    /**
     * The puzzle instance is solvable if match one of these conditions:
     * 1. If 'dimension' is odd, then puzzle instance is solvable if number of inversions is even in the input state.
     * 2. If 'dimension' is even, puzzle instance is solvable if:
     *     a. the blank is on an even row counting from the bottom (second-last, fourth-last, etc.) and number of inversions is odd.
     *     b. the blank is on an odd row counting from the bottom (last, third-last, fifth-last, etc.) and number of inversions is even.
     * For all other cases, the puzzle instance is not solvable.
     */
    private fun isSolvable(pieces: List<Piece>): Boolean {
        if (pieces.map { it.value }.isSorted()) {
            return false
        }
        val inversions = countInversions(pieces)
        return if (dimension.isOdd()) {
            inversions.isEven()
        } else {
            val emptyPieceRow = getEmptyPiecePositionFromBottom(pieces)
            ((emptyPieceRow.isEven() && inversions.isOdd()) || (emptyPieceRow.isOdd() && inversions.isEven()))
        }
    }

    private fun countInversions(pieces: List<Piece>): Int {
        var inversions = 0
        pieces.forEachIndexed { index, piece ->
            if (piece.bitmap != null) {
                for (nextIndex in index.plus(1) until dimension.times(dimension)) {
                    if (piece.value > pieces[nextIndex].value) inversions++
                }
            }
        }
        return inversions
    }

    private fun getEmptyPiecePositionFromBottom(pieces: List<Piece>): Int {
        val emptyRow = pieces.withIndex().filter {
            it.value.bitmap == null
        }.map {
            findPositionIn2DArray(it.index).first
        }.first()
        return dimension.minus(emptyRow)
    }

    // endregion

    private fun findPositionIn2DArray(position: Int): Pair<Int, Int> {
        val row = position / dimension
        val column = position % dimension
        return Pair(row, column)
    }

    // region Move Pieces

    fun movePiece(position: Int): Boolean {
        if (isSolved) return false
        findPositionIn2DArray(position).run {
            // Can not move the empty piece
            if (pieces[first][second].bitmap == null) return false
            return move(this, Position.TOP)
                    || move(this, Position.BOTTOM)
                    || move(this, Position.LEFT)
                    || move(this, Position.RIGHT)
        }
    }

    private fun move(origin2D: Pair<Int, Int>, position: Position): Boolean {
        val row = origin2D.first
        val column = origin2D.second
        return when (position) {
            Position.TOP -> switchPieces(origin2D, Pair(row - 1, column))
            Position.BOTTOM -> switchPieces(origin2D, Pair(row + 1, column))
            Position.LEFT -> switchPieces(origin2D, Pair(row, column - 1))
            Position.RIGHT -> switchPieces(origin2D, Pair(row, column + 1))
        }
    }

    private fun switchPieces(origin2D: Pair<Int, Int>, destination2D: Pair<Int, Int>): Boolean {
        if (destination2D.first in 0 until dimension && destination2D.second in 0 until dimension) {
            if (pieces[destination2D.first][destination2D.second].bitmap == null) {
                val pieceOrigin = pieces[origin2D.first][origin2D.second]
                pieces[origin2D.first][origin2D.second] = pieces[destination2D.first][destination2D.second]
                pieces[destination2D.first][destination2D.second] = pieceOrigin
                isSolved = isPuzzleSolved()
                return true
            }
        }
        return false
    }

    private fun isPuzzleSolved(): Boolean {
        pieces.forEachIndexed { row, piecesByRow ->
            piecesByRow.forEachIndexed { column, piece ->
                if (piece.originalRow != row || piece.originalColumn != column) return false
            }
        }
        return true
    }

    // endregion

}