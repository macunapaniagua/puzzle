package net.golabs.puzzle.models

import android.graphics.Bitmap

data class Piece(val bitmap: Bitmap?,
                 val value: Int,
                 val originalRow: Int,
                 val originalColumn: Int)